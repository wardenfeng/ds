module feng.ds {
	/**
	 * 迭代器
	 * @author feng 2016-4-17
	 */
    export interface Iterator<T> {
		/**
		 * 返回当前元素并且指针移到下一个元素
		 */
        next(): T;

		/**
		 * 是否有下一个元素
		 */
        hasNext(): boolean

		/**
		 * 移到起始位置
		 */
        start(): void;

		/**
		 * 指向的当前元素
		 */
        data: T;
    }
}