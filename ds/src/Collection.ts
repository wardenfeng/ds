module feng.ds {

    /**
     * 集合
	 * @author feng 2016-4-17
     */
    export interface Collection<T> {
		/**
         * 是否包含指定元素
		 * @param item       元素
		 */
        contains(item:T): boolean;

		/**
		 * 清理
		 */
        clear(): void;

		/**
         * 获取一个迭代器，用于遍历集合
		 */
        getIterator(): Iterator<T>;

		/**
		 * 元素数量
		 */
        count: number;

		/**
		 * 集合尺寸
		 */
		size: number;

		/**
		 * 输出所有元素到数组
		 */
        toArray(): T[];
    }
}