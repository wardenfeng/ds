module feng.ds.compare {

    /**
     * 数字排序
     * @param   a   数字a
     * @param   b   数字b
     * @return      小于0：a在前；等于0：顺序不变；大于0：b在前
     */
    export function compareNumber(a: number, b: number): number {
        return a - b;
    }
}