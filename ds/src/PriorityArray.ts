module feng.ds {
    /**
     * 优先数组
     * @author feng 2016-4-17
     */
    export class PriorityArray<T> implements Collection<T>
    {
        /**
         * 数据
         */
        private data: T[];

        /**
         * 比较函数
         */
        private compare: (a: T, b: T) => number;

        /**
         * 构建优先数组
         * @param   compare     比较函数
         */
        constructor(compare: (a: T, b: T) => number) {

            this.data = [];
            this.compare = compare;
        }

        /**
         * 是否包含指定元素
		 * @param item       元素
		 */
        public contains(item: T): boolean {

            for (var i = 0; i < this.data.length; i++) {
                var element = this.data[i];
                if (element == item)
                    return true;
            }
            return false;
        }

        /**
         * 清理
         */
        public clear(): void {
            this.data.length = 0;
        }

        /**
         * 获取优先数组迭代器
		 */
        public getIterator(): PriorityArrayIterator<T> {
            return new PriorityArrayIterator<T>(this);
        }

        /**
		 * 元素数量
		 */
        public get count(): number {
            return this.data.length;
        }

		/**
		 * 集合尺寸
		 */
        public get size(): number {
            return this.data.length;
        }

        /**
		 * 输出所有元素到数组
		 */
        public toArray(): T[] {
            return this.data.concat();
        }

        /**
         * 获取元素
         * @param   index   索引
         */
        public getAt(index: number): T {
            return this.data[index];
        }
    }
}