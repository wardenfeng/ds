module feng.ds {

    /**
     * 优先数组迭代器
     * @author feng 2016-4-17
     */
    export class PriorityArrayIterator<T> implements Iterator<T>
    {
        /**
         * 优先数组
         */
        private priorityArray: PriorityArray<T>;

        /**
         * 光标、指针
         */
        private cursor: number;

        /**
         * 构建优先数组迭代器
         * @param priorityArray     优先数组
         */
        constructor(priorityArray: PriorityArray<T>) {
            this.priorityArray = priorityArray;
        }

        /**
		 * 返回当前元素并且指针移到下一个元素
		 */
        public next(): T {
            var value = this.priorityArray.getAt(this.cursor);
            this.cursor++;
            return value;
        }

		/**
		 * 是否有下一个元素
		 */
        public hasNext(): boolean {
            return this.cursor < this.priorityArray.size;
        }

		/**
		 * 移到起始位置
		 */
        public start(): void {
            this.cursor = 0;
        }

		/**
		 * 指向的当前元素
		 */
        public get data(): T {
            return this.priorityArray.getAt(this.cursor);
        }
    }
}